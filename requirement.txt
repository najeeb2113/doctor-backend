click==7.1.2
Flask==1.1.2
Flask-Login==0.5.0
flask-marshmallow==0.14.0
itsdangerous==1.1.0
Jinja2==2.11.3
MarkupSafe==1.1.1
marshmallow==3.10.0
marshmallow-sqlalchemy==0.24.2
PyMySQL==1.0.2
six==1.15.0
SQLAlchemy==1.3.23
Werkzeug==1.0.1
