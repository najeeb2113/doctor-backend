Header for all request has to be made 
````
add header 
X-AUTH : abc123
````

FOR EVERY ACTIVITY DONE BY USER SHOULD LOG into Activity DB:
add activity for notificaitons
````
http://host/addactivity     (POST)
inputs:
    user_id,activity
output:
    Success
````

fetch activity for notification bar

````
http://host/activitydetails   (GET)
inputs :
    nil
output:
 {
  "activity": [
    {
      "activity": "user created succesfully", 
      "id": 3, 
      "ts": "2021-03-02 16:29:47", 
      "user": {
        "dateofsub": "2021-03-02 16:29:33", 
        "email": "samrat_majumdar76@gmail.com", 
        "id": 2, 
        "password": "2113", 
        "phone": 8555051922, 
        "role": "Admin", 
        "speciality": "Breast Cancer", 
        "username": "samrat majumdar"
      }, 
      "user_id": 2
    }, 
    {
      "activity": "user created succesfully", 
      "id": 2, 
      "ts": "2021-03-02 16:29:47", 
      "user": {
        "dateofsub": "2021-03-02 16:29:33", 
        "email": "najeebaddu@gmail.com", 
        "id": 1, 
        "password": "2113", 
        "phone": 8121507936, 
        "role": "Doctor", 
        "speciality": "Breast Cancer", 
        "username": "najeeb pasha"
      }, 
      "user_id": 1
    }
  ]
}
````
For User Login:
````
http://host/userlogin      (POST)

INPUT:
    email,password
OUTPUT:
{
  "user": [
    {
      "dateofsub": "2021-02-27T02:56:35", 
      "email": "najeebcheck@gmail.com", 
      "id": 1, 
      "password": "2113", 
      "phone": 8555051922, 
      "role": "admin", 
      "speciality": "Breast Cancer", 
      "username": "najeeb"
    } 
  ]
}
, Fail
````

For Fetch All user details for umanagment table:

````
  http://host/alluserdetails     (GET)
  inputs:
        nil
  output:
{
  "user": [
    {
      "dateofsub": "2021-03-02 16:29:33", 
      "email": "najeebaddu@gmail.com", 
      "id": 1, 
      "password": "2113", 
      "phone": 8121507936, 
      "role": "Doctor", 
      "speciality": "Breast Cancer", 
      "username": "najeeb pasha"
    }, 
    {
      "dateofsub": "2021-03-02 16:29:33", 
      "email": "samrat_majumdar76@gmail.com", 
      "id": 2, 
      "password": "2113", 
      "phone": 8555051922, 
      "role": "Admin", 
      "speciality": "Breast Cancer", 
      "username": "samrat majumdar"
    }
  ]
}
````

For Fetch All user details for Pmanagement table:
Rheumapatient table
````
  http://host/allrpatientdetails     (GET)
  inputs:
        nil

  output:
{
  "rheumatology_patient": [
    {
      "a_tc_s1": "0", 
      "a_tc_s2": "0", 
      "a_tc_t1": "0", 
      "a_tc_t2": "0", 
      "a_tt_s1": "0", 
      "a_tt_s2": "0", 
      "a_tt_t1": "0", 
      "a_tt_t2": "0", 
      "abdomen_examination": "0", 
      "abortions": "0", 
      "abortions_cause": "0", 
      "abortions_number": 0, 
      "acl_s1": "0", 
      "pname": "najeeb"
    },
    {
      "a_tc_s1": "0", 
      "a_tc_s2": "0", 
      "a_tc_t1": "0", 
      "a_tc_t2": "0", 
      "a_tt_s1": "0", 
      "a_tt_s2": "0", 
      "a_tt_t1": "0", 
      "a_tt_t2": "0", 
      "abdomen_examination": "0", 
      "abortions": "0", 
      "abortions_cause": "0", 
      "abortions_number": 0, 
      "acl_s1": "0", 
      "pname": "najeeb"
    }
]
}
 ,fail    
````
breast-patient table
````
  http://host/allbpatientdetails     (GET)
  inputs:
        nil

  output:
{
  "breast_patient": [
    {
      "abortions": "1", 
      "abortions_cause": "something", 
      "abortions_number": 1, 
      "addiction": "no", 
      "address": "My house", 
      "age": 100, 
      "alcohol": "yes", 
      "alcohol_years": 2, 
      "appetite": "abnormal", 
      "area": "Uppal", 
      "axillarylump": "no", 
      "axillarylump_side": "not applicable", 
      "axillarylump_size": "2.5", 
      "bladderhabit": "no", 
      "blood_investigation": "no thanks", 
      "bmi": "120", 
      "pname": "prashanthi"
    }, 
    {
      "abortions": "yes", 
      "abortions_cause": "2", 
      "abortions_number": 2, 
      "addiction": "yes", 
      "address": "vkb", 
      "age": 12, 
      "alcohol": "yes", 
      "alcohol_years": 2, 
      "appetite": "0", 
      "area": "urban", 
      "axillarylump": "0", 
      "axillarylump_side": "0", 
      "axillarylump_size": "0", 
      "bladderhabit": "0",
      "blood_investigation": "no thanks", 
      "bmi": "120", 
      "pname": "prejwalitha"
    } 
]
}

````

For Fetch current user details

````
  http://host/userdetails     (GET)
  inputs:
        uid
  output:
{
  "user": [
    {
      "dateofsub": "2021-02-27T02:56:35", 
      "email": "najeebcheck@gmail.com", 
      "id": 1, 
      "password": "2113", 
      "phone": 8555051922, 
      "role": "admin", 
      "speciality": "Breast Cancer", 
      "username": "najeeb"
    } 
  ]
}
,Fail
````
For adding user:

````
http://host/adduser        (POST)
inputs:
    uname,phone,email,role,password,speciality
output:
    Success,Fail
````

For updating user details
````
http://host/userupdate    (POST)

**INPUT:**
    user_id,username,phone,role,speciality,password,email

**OUTPUT**

    {
"user":{
"dateofsub": "2021-03-02 16:29:33",
"email": "najeebaddu@gmail.com",
"id": 1,
"password": "2113",
"phone": 8121507936,
"role": "Doctor",
"speciality": "Breast Cancer",
"username": "najeeb pasha"
}
}
````
For userdelete
````
http://host/userdelete    (Get)

INPUT:
    UID
OUTPUT:
    "Success","Fail"
````

For adding breast patient

````
http://host/addbpatient    (POST)
**INPUT:**
    pid = Column(BigInteger, nullable=False,unique=True)
    date = Column(TIMESTAMP, server_default=text('CURRENT_TIMESTAMP'), nullable=True)
    code = Column(String(50), nullable=False)
    place = Column(String(60), nullable=False)
    pname = Column(String(60), nullable=False)
    fhname = Column(String(30), nullable=False)
    age = Column(Integer, nullable=False)
    sex  = Column(String(10), nullable=False)
    education  = Column(String(40), nullable=False)
    address = Column(String(250), nullable=False)
    phone = Column(BigInteger, nullable=False)
    district = Column(String(30), nullable=False)
    state = Column(String(25), nullable=False)
    area = Column(String(30), nullable=False)
    referred_by = Column(String(40), nullable=False,default="None")
    occupation = Column(String(40), nullable=False)
    ethnic = Column(String(30), nullable=False)
    marital_status = Column(String(30), nullable=False)
    marital_status_years = Column(BigInteger, nullable=False,default="0")
    menses_frequency = Column(String(20), nullable=False)
    menses_loss = Column(String(20), nullable=False)
    menarche_years = Column(BigInteger, nullable=False,default="0")
    hystrectomy = Column(String(15), nullable=False)
    hystrectomy_years = Column(BigInteger, nullable=False,default="0")
    menopause = Column(String(15), nullable=False)
    menopause_years = Column(BigInteger, nullable=False,default="0")
    children = Column(Integer, nullable=False)
    children_male = Column(Integer, nullable=False,default="0")
    children_female = Column(Integer, nullable=False,default="0")
    abortions = Column(String(15), nullable=False)
    abortions_number = Column(Integer, nullable=False,default="0")
    abortions_cause = Column(String(25), nullable=False,default="nil")
    breastfed = Column(String(15), nullable=False)
    breastfed_years = Column(Integer, nullable=False,default="0")
    current_lactation = Column(String(15), nullable=False)
    contraception_methods = Column(String(15), nullable=False)
    contraception_methods_type = Column(String(25), nullable=False,default="nil")
    hormone_treatment = Column(String(15), nullable=False)
    addiction = Column(String(20), nullable=False,default="nil")
    tobacco = Column(String(15), nullable=False)
    tobacco_years = Column(Integer, nullable=False,default="0")
    smoking = Column(String(15), nullable=False)
    smoking_years = Column(Integer, nullable=False,default="0")
    alcohol = Column(String(15), nullable=False)
    alcohol_years = Column(Integer, nullable=False, default="0")
    family_history = Column(String(50), nullable=False)
    comorbidities = Column(String(50), nullable=False)
    breastlump = Column(String(15), nullable=False)
    breastlump_location = Column(String(50), nullable=False)
    breastlump_size = Column(String(30), nullable=False)
    overlying_skin = Column(String(50), nullable=False)
    axillarylump = Column(String(30), nullable=False)
    axillarylump_side = Column(String(15), nullable=False)
    matted = Column(String(10), nullable=False)
    axillarylump_size = Column(String(35), nullable=False)
    nipple_discharge = Column(String(15), nullable=False)
    nipple_discharge_frequency = Column(String(25), nullable=False)
    nipple_discharge_colour = Column(String(25), nullable=False)
    mastalgia = Column(String(35), nullable=False)
    mastitis = Column(String(35), nullable=False)
    ulcer = Column(String(35), nullable=False)
    nipple_inversion = Column(String(35), nullable=False)
    others = Column(String(50), nullable=False)
    duration = Column(String(25), nullable=False)
    pasthistory = Column(String(50), nullable=False)
    surgical_history = Column(String(50), nullable=False)
    drug_history = Column(String(50), nullable=False)
    drug_allergy = Column(String(15), nullable=False)
    drug_allergy_type = Column(String(40), nullable=True)
    bowelhabit = Column(String(25), nullable=False)
    bladderhabit = Column(String(25), nullable=False)
    sleep = Column(String(20), nullable=False)
    appetite = Column(String(50), nullable=False)
#examination
    weight = Column(Integer, nullable=False)
    height = Column(Integer, nullable=False)
    bmi = Column(String(30), nullable=False)
    bp = Column(Integer, nullable=False)
    pulse = Column(Integer, nullable=False)
    temp = Column(String(25), nullable=False)
    respiratory_rate = Column(Integer, nullable=False)
    health_condition = Column(String(50), nullable=False)
    examination_remarks = Column(String(50), nullable=False)
    bra_size = Column(String(150), nullable=False)
# investigations
    usg = Column(String(50), nullable=True)
    mmg = Column(String(50), nullable=True)
    mri = Column(String(50), nullable=True)
    fnac = Column(String(50), nullable=True)
    core_biopsy = Column(String(50), nullable=True)
    incision_biopsy = Column(String(50), nullable=True)
    investigation_remarks = Column(String(50), nullable=True)
    blood_investigation = Column(String(250), nullable=True)
    diagnosis = Column(String(50), nullable=True)
    treatment_plan = Column(String(50), nullable=True)

    user_id = Column(BigInteger, ForeignKey('users.id'))

**OUTPUT:**
    Success,Fail
````

FOR FETCHING BREAST PATIENT DETAILS

````
http://host/bpatientdetails     (GET)
inputs:
    pid
output:
{
    "bpatient": {
        "abortions": "1",
        "abortions_cause": "something",
        "abortions_number": 1,
        "addiction": "no",
        "address": "My house",
        "age": 100,
        "alcohol": "yes",
        "alcohol_years": 2,
        "appetite": "abnormal",
        "area": "Uppal",
        "axillarylump": "no",
        "axillarylump_side": "not applicable",
        "axillarylump_size": "2.5",
        "bladderhabit": "no",
        "blood_investigation": "no thanks",
        "bmi": "120",
        "bowelhabit": "no",
        "bp": 98,
        "bra_size": "34",
        "breastfed": "no",
        "breastfed_years": 0,
        "breastlump": "no",
        "breastlump_location": "i dont know",
        "breastlump_size": "89",
        "children": 0,
        "children_female": 0,
        "children_male": 0,
        "code": "1234",
        "comorbidities": "i dont know",
        "contraception_methods": "pills",
        "contraception_methods_type": "pills",
        "core_biopsy": "no thanks",
        "current_lactation": "hyderabad",
        "date": "2021-03-05 02:58:26",
        "diagnosis": "no thanks",
        "district": "Hyderabad",
        "drug_allergy": "yes",
        "drug_allergy_type": "pencilin",
        "drug_history": "no",
        "duration": "1 hour",
        "education": "M Pharmacy",
        "ethnic": "sikh",
        "examination_remarks": "very bad ulcer full corrupt body very galiz",
        "family_history": "not applicable",
        "fhname": "unknown",
        "fnac": "no thanks",
        "health_condition": "very good",
        "height": 148,
        "hormone_treatment": "no",
        "hystrectomy": "i dont know",
        "hystrectomy_years": 2,
        "incision_biopsy": "no thanks",
        "investigation_remarks": "no thanks",
        "marital_status": "married",
        "marital_status_years": 20,
        "mastalgia": "i dont know",
        "mastitis": "i dont know",
        "matted": "no",
        "menarche_years": 2,
        "menopause": "i dont know",
        "menopause_years": 2,
        "menses_frequency": "regular",
        "menses_loss": "heavy",
        "mmg": "no thanks",
        "mri": "no thanks",
        "nipple_discharge": "no",
        "nipple_discharge_colour": "white",
        "nipple_discharge_frequency": "normal",
        "nipple_inversion": "donno",
        "occupation": "Engineering",
        "others": "not applicable",
        "overlying_skin": "no",
        "pasthistory": "no",
        "phone": 9876543210,
        "pid": 1,
        "place": "hyderabad",
        "pname": "unknown",
        "pulse": 72,
        "referred_by": "Myself",
        "respiratory_rate": 56,
        "sex": "M",
        "sleep": "adequate",
        "smoking": "no",
        "smoking_years": 0,
        "state": "Telangana",
        "surgical_history": "no",
        "temp": "99",
        "tobacco": "no",
        "tobacco_years": 0,
        "treatment_plan": "no thanks",
        "ulcer": "no",
        "user": {
            "dateofsub": "2021-03-02 16:29:33",
            "email": "najeebaddu@gmail.com",
            "id": 1,
            "password": "2113",
            "phone": 8121507936,
            "role": "Doctor",
            "speciality": "Breast Cancer",
            "username": "najeeb pasha"
        },
        "usg": "no thanks",
        "weight": 48
    }
}

````
For updating Breast patient details:
````
http://host/updatebpatient     (POST)
inputs :
    all the breast patient fields 
output :
    updated details
{
    "bpatient": {
        "abortions": "1",
        "abortions_cause": "something",
        "abortions_number": 1,
        "addiction": "no",
        "address": "My house",
        "age": 100,
        "alcohol": "yes",
        "alcohol_years": 2,
        "appetite": "abnormal",
        "area": "Uppal",
        "axillarylump": "no",
        "axillarylump_side": "not applicable",
        "axillarylump_size": "2.5",
        "bladderhabit": "no",
        "blood_investigation": "no thanks",
        "bmi": "120",
        "bowelhabit": "no",
        "bp": 98,
        "bra_size": "34",
        "breastfed": "no",
        "breastfed_years": 0,
        "breastlump": "no",
        "breastlump_location": "i dont know",
        "breastlump_size": "89",
        "children": 0,
        "children_female": 0,
        "children_male": 0,
        "code": "1234",
        "comorbidities": "i dont know",
        "contraception_methods": "pills",
        "contraception_methods_type": "pills",
        "core_biopsy": "no thanks",
        "current_lactation": "hyderabad",
        "date": "2021-03-05 02:58:26",
        "diagnosis": "no thanks",
        "district": "Hyderabad",
        "drug_allergy": "yes",
        "drug_allergy_type": "pencilin",
        "drug_history": "no",
        "duration": "1 hour",
        "education": "M Pharmacy",
        "ethnic": "sikh",
        "examination_remarks": "very bad ulcer full corrupt body very galiz",
        "family_history": "not applicable",
        "fhname": "unknown",
        "fnac": "no thanks",
        "health_condition": "very good",
        "height": 148,
        "hormone_treatment": "no",
        "hystrectomy": "i dont know",
        "hystrectomy_years": 2,
        "incision_biopsy": "no thanks",
        "investigation_remarks": "no thanks",
        "marital_status": "married",
        "marital_status_years": 20,
        "mastalgia": "i dont know",
        "mastitis": "i dont know",
        "matted": "no",
        "menarche_years": 2,
        "menopause": "i dont know",
        "menopause_years": 2,
        "menses_frequency": "regular",
        "menses_loss": "heavy",
        "mmg": "no thanks",
        "mri": "no thanks",
        "nipple_discharge": "no",
        "nipple_discharge_colour": "white",
        "nipple_discharge_frequency": "normal",
        "nipple_inversion": "donno",
        "occupation": "Engineering",
        "others": "not applicable",
        "overlying_skin": "no",
        "pasthistory": "no",
        "phone": 9876543210,
        "pid": 1,
        "place": "hyderabad",
        "pname": "unknown",
        "pulse": 72,
        "referred_by": "Myself",
        "respiratory_rate": 56,
        "sex": "M",
        "sleep": "adequate",
        "smoking": "no",
        "smoking_years": 0,
        "state": "Telangana",
        "surgical_history": "no",
        "temp": "99",
        "tobacco": "no",
        "tobacco_years": 0,
        "treatment_plan": "no thanks",
        "ulcer": "no",
        "user": {
            "dateofsub": "2021-03-02 16:29:33",
            "email": "najeebaddu@gmail.com",
            "id": 1,
            "password": "2113",
            "phone": 8121507936,
            "role": "Doctor",
            "speciality": "Breast Cancer",
            "username": "najeeb pasha"
        },
        "usg": "no thanks",
        "weight": 48
    }
}
````
For Deleting Breast patient details:
````
http://host/bpatientdelete     (GET)

Inputs:
    PID
Output:
    Success,Fail
````
For getting rheumatology patient details:
````
http://host/rheumapatientdetails     (GET)

Inputs:
    PID
Output:
    {
  "RheumatologyPatient": {
    "a_tc_s1": null, 
    "a_tc_s2": null, 
    "a_tc_t1": null, 
    "a_tc_t2": null, 
    "a_tt_s1": null, 
    "a_tt_s2": null, 
    "a_tt_t1": null, 
    "a_tt_t2": null, 
    "abdomen_examination": "no", 
    "abortions": "yes", 
    "abortions_cause": "ipill", 
    "abortions_number": 2, 
    "acl_s1": "no", 
    "acl_s2": "no", 
    "acl_t1": "yes", 
    "acl_t2": "yes", 
    "addiction": "yes", 
    "address": "hyderabad", 
    "age": 12, 
    "alcohol": "YES", 
    "alcohol_years": 6, 
    "ankle_lt": null, 
    "ta_lt": null, 
    "ta_rt": null, 
    "temp": "98", 
    "tender": "najeeb", 
    "tender_type": "manshi", 
    "thorasic_anky": null, 
    "thorasic_ext": null, 
    "thorasic_flex": null, 
    "thorasic_ltfl": null, 
    "thorasic_ltrot": null, 
    "thorasic_pt": null, 
    "thorasic_rtfl": null, 
    "thorasic_rtrot": null, 
    "thumb_lt": null, 
    "thumb_rt": null, 
    "tm_s1": "no", 
    "tm_s2": "no", 
    "tm_t1": "yes", 
    "tm_t2": "yes", 
    "tobacco": "no", 
    "tobacco_years": 0, 
    "trcr_lt": null, 
    "trcr_rt": null, 
    "treatment_plan": null, 
    "trpz_lt": null, 
    "trpz_rt": null, 
    "user": {
      "dateofsub": "2021-03-02 16:29:33", 
      "email": "najeebaddu@gmail.com", 
      "id": 1, 
      "password": "2113", 
      "phone": 8121507936, 
      "role": "Doctor", 
      "speciality": "Breast Cancer", 
      "username": "najeeb pasha"
    }, 
    "user_id": 1, 
    "usg": null, 
    "vascular": "no", 
    "vdi": null, 
    "weight": 46, 
  }
}
#if fail:
    wrong pid 
above is the half of the output
````
For Rheuma Patient Delete:
````
http://host/rheumapatientdelete     (GET)

Inputs:
    PID
Output:
    Success,Fail
````
FOR RHEUMA PATIENT ADD :
````
http://host/addrheumapatient       (POST)

Inputs:
    pid,date,code,place,pname,fhname,age,sex,education,address,phone,district,state,area,referred_by,occupation,ethnic,marital_status,marital_status_years,menses_frequency,menses_loss,menarche_years,hysterectomy,hysterectomy_years,menopause,menopause_years,children,children_male,children_female,abortions,abortions_number,abortions_cause,current_lactation,contraception_methods,contraception_methods_type,hormone_treatment,addiction,tobacco,tobacco_years,smoking,smoking_years,alcohol,alcohol_years,family_history,health_condition,remarks,comorbidities,onset,onset_duration,onset_duration_type,first_symptom,initial_site_joint,soft_tissue,others,course1,course2,pattern1,pattern2,current_relapse,current_relapse_type,detailed_history,pasthistory,surgical_history,drug_history,drug_allergy,drug_allergy_type,bowelhabit,sleep,general_condition,weight,height,bmi,bp,pulse,temp,respiratory_rate,ex_health_condition,ex_remarks,eyes,skin_morphology,skin_pattern,skin_color,distribution,other_affects,mucosa,hair_loss,dandruff,nail,lymphnodes,lymphnodes_number,tender,tender_type,lymphnodes_tender_others,vascular,cns_examination,cvs_examination,chest_examination,abdomen_examination,tm_t1,tm_s1,tm_t2,tm_s2,scl_t1,scl_s1,scl_t2,scl_s2,acl_t1,acl_s1,acl_t2,acl_s2,sh_t1,sh_s1,sh_t2,sh_s2,elbow_t1,elbow_s1,elbow_t2,elbow_s2,infru_t1,infru_s1,infru_t2,infru_s2,cmc1_t1,cmc1_s1,cmc1_t2,cmc1_s2,wrist_t1,wrist_s1,wrist_t2,wrist_s2,dip2_t1,dip2_s1,dip2_t2,dip2_s2,dip3_t1,dip3_s1,dip3_t2,dip3_s2,dip4_t1,dip4_s1,dip4_t2,dip4_s2,dip5_t1,dip5_s1,dip5_t2,dip5_s2,ip1_t1,ip1_s1,ip1_t2,ip1_s2,ip2_t1,ip2_s1,ip2_t2,ip2_s2,pip3_t1,pip3_s1,pip3_t2,pip3_s2,pip4_t1,pip4_s1,pip4_t2,pip4_s2,pip5_t1,pip5_s1,pip5_t2,pip5_s2,mcp1_t1,mcp1_s1,mcp1_t2,mcp1_s2,mcp2_t1,mcp2_s1,mcp2_t2,mcp2_s2,mcp3_t1,mcp3_s1,mcp3_t2,mcp3_s2,mcp4_t1,mcp4_s1,mcp4_t2,mcp4_s2,mcp5_t1,mcp5_s1,mcp5_t2,mcp5_s2,hip_t1,hip_s1,hip_t2,hip_s2,knee_t1,knee_s1,knee_t2,knee_s2,a_tt_t1,a_tt_s1,a_tt_t2,a_tt_s2,a_tc_t1,a_tc_s1,a_tc_t2,a_tc_s2,mtl_t1,mtl_s1,mtl_t2,mtl_s2,mtp1_t1,mtp1_s1,mtp1_t2,mtp1_s2,mtp2_t1,mtp2_s1,mtp2_t2,mtp2_s2,mtp3_t1,mtp3_s1,mtp3_t2,mtp3_s2,mtp4_t1,mtp4_s1,mtp4_t2,mtp4_s2,mtp5_t1,mtp5_s1,mtp5_t2,mtp5_s2,ip1b_t1,ip1b_s1,ip1b_t2,ip1b_s2,ip2b_t1,ip2b_s1,ip2b_t2,ip2b_s2,ip3b_t1,ip3b_s1,ip3b_t2,ip3b_s2,ip4b_t1,ip4b_s1,ip4b_t2,ip4b_s2,ip5b_t1,ip5b_s1,ip5b_t2,ip5b_s2,s1_t1,s1_s1,s1_t2,s1_s2,cervical_anky,cervical_flex,cervical_ext,cervical_rtrot,cervical_ltrot,cervical_rtfl,cervical_ltfl,cervical_pt,thorasic_anky,thorasic_flex,thorasic_ext,thorasic_rtrot,thorasic_ltrot,thorasic_rtfl,thorasic_ltfl,thorasic_pt,lumbar_anky,lumbar_flex,lumbar_ext,lumbar_rtrot,lumbar_ltrot,lumbar_rtfl,lumbar_ltfl,lumbar_pt,opt_rt,opt_lt,lcer_rt,lcer_lt,trpz_rt,trpz_lt,scap_rt,scap_lt,zcst_rt,zcst_lt,epdl_rt,epdl_lt,glut_rt,glut_lt,trcr_rt,trcr_lt,knee_rt,knee_lt,ta_rt,ta_lt,calf_rt,calf_lt,sole_rt,sole_lt,rhand_fl,rhand_ex,rhand_ab,rhand_add,lhand_fl,lhand_ex,lhand_ab,lhand_add,rwrist_fl,rwrist_ex,rwrist_ab,rwrist_add,lwrist_fl,lwrist_ex,lwrist_ab,lwrist_add,relb_fl,relb_ex,relb_ab,relb_add,shrt_fl,shrt_ex,shrt_ab,shrt_add,shlt_fl,shlt_ex,shlt_ab,shlt_add,kneert_fl,kneert_ex,kneert_ab,kneert_add,kneelt_fl,kneelt_ex,kneelt_ab,kneelt_add,footrt_fl,footrt_ex,footrt_ab,footrt_add,footlt_fl,footlt_ex,footlt_ab,footlt_add,thumb_rt,thumb_lt,finger_rt,finger_lt,palm_rt,palm_lt,elbow_rt,elbow_lt,hy_knee_rt,hy_knee_lt,ankle_rt,ankle_lt,other_rt,other_lt,spine_rt,spine_lt,can_u_dress_urself,can_u_wash_ur_hair,can_u_comb_ur_hair,can_u_stand_from_chair,can_u_get_inout_from_bed,can_u_sit_grossteg_onfloor,can_u_cut_vegetables,can_u_lift_glass,can_u_break_roti_from_1hand,can_u_walk,can_u_climb_5steps,can_u_take_bath,can_u_wash_dry_urbody,can_u_get_onoff_toilet,can_u_weigh_2kg,can_u_bend_and_pickcloths,can_u_open_bottle,can_u_turntaps_onoff,can_u_open_latches,can_u_work_office_house,can_u_run_errands,can_u_get_inout_of_bus,patient_assessment_pain,grip_strength_rt,grip_strength_hg,grip_strength_lt,early_mrng_stiffness,assess_sleep,general_health_assessment,classification_criteria_followig,diagnosis,treatment_plan,hematological_date,hem_esr,hem_hb,hem_tlc,hem_pleb,hem_plat,hem_urine,hem_others,hematological_date2,hem_esr2,hem_hb2,hem_tlc2,hem_pleb2,hem_plat2,hem_urine2,hem_others2,hematological_date3,hem_esr3,hem_hb3,hem_tlc3,hem_pleb3,hem_plat3,hem_urine3,hem_others3,hematological_date4,hem_esr4,hem_hb4,hem_tlc4,hem_pleb4,hem_plat4,hem_urine4,hem_others4,hematological_date5,hem_esr5,hem_hb5,hem_tlc5,hem_pleb5,hem_plat5,hem_urine5,hem_others5,biochemical_date,bio_lft,bio_rft,bio_crp,bio_bsl,bio_ua,bio_others,biochemical_date2,bio_lft2,bio_rft2,bio_crp2,bio_bsl2,bio_ua2,bio_others2,biochemical_date3,bio_lft3,bio_rft3,bio_crp3,bio_bsl3,bio_ua3,bio_others3,biochemical_date4,bio_lft4,bio_rft4,bio_crp4,bio_bsl4,bio_ua4,bio_others4,biochemical_date5,bio_lft5,bio_rft5,bio_crp5,bio_bsl5,bio_ua5,bio_others5,immunological_date,imm_rf,imm_accp,imm_ana,imm_anawb,imm_ace,imm_dsdna,imm_hlab27,imm_c3,imm_c4,imm_others,immunological_date2,imm_rf2,imm_accp2,imm_ana2,imm_anawb2,imm_ace2,imm_dsdna2,imm_hlab272,imm_c32,imm_c42,imm_others2,immunological_date3,imm_rf3,imm_accp3,imm_ana3,imm_anawb3,imm_ace3,imm_dsdna3,imm_hlab273,imm_c33,imm_c43,imm_others3,usg,xray_chest,xray_joints,mri,radio_remarks,das28,sledai,basdai,pasi,bvas,mrss,sdai_cdai,asdas,dapsa,vdi,essdai,user_id,
Output:
    Success,Fail
````
FOR RHEUMA PATIENT UPDATE :
````
http://host/updaterheumapatient       (POST)

Inputs:
    id,pid,date,code,place,pname,fhname,age,sex,education,address,phone,district,state,area,referred_by,occupation,ethnic,marital_status,marital_status_years,menses_frequency,menses_loss,menarche_years,hysterectomy,hysterectomy_years,menopause,menopause_years,children,children_male,children_female,abortions,abortions_number,abortions_cause,current_lactation,contraception_methods,contraception_methods_type,hormone_treatment,addiction,tobacco,tobacco_years,smoking,smoking_years,alcohol,alcohol_years,family_history,health_condition,remarks,comorbidities,onset,onset_duration,onset_duration_type,first_symptom,initial_site_joint,soft_tissue,others,course1,course2,pattern1,pattern2,current_relapse,current_relapse_type,detailed_history,pasthistory,surgical_history,drug_history,drug_allergy,drug_allergy_type,bowelhabit,sleep,general_condition,weight,height,bmi,bp,pulse,temp,respiratory_rate,ex_health_condition,ex_remarks,eyes,skin_morphology,skin_pattern,skin_color,distribution,other_affects,mucosa,hair_loss,dandruff,nail,lymphnodes,lymphnodes_number,tender,tender_type,lymphnodes_tender_others,vascular,cns_examination,cvs_examination,chest_examination,abdomen_examination,tm_t1,tm_s1,tm_t2,tm_s2,scl_t1,scl_s1,scl_t2,scl_s2,acl_t1,acl_s1,acl_t2,acl_s2,sh_t1,sh_s1,sh_t2,sh_s2,elbow_t1,elbow_s1,elbow_t2,elbow_s2,infru_t1,infru_s1,infru_t2,infru_s2,cmc1_t1,cmc1_s1,cmc1_t2,cmc1_s2,wrist_t1,wrist_s1,wrist_t2,wrist_s2,dip2_t1,dip2_s1,dip2_t2,dip2_s2,dip3_t1,dip3_s1,dip3_t2,dip3_s2,dip4_t1,dip4_s1,dip4_t2,dip4_s2,dip5_t1,dip5_s1,dip5_t2,dip5_s2,ip1_t1,ip1_s1,ip1_t2,ip1_s2,ip2_t1,ip2_s1,ip2_t2,ip2_s2,pip3_t1,pip3_s1,pip3_t2,pip3_s2,pip4_t1,pip4_s1,pip4_t2,pip4_s2,pip5_t1,pip5_s1,pip5_t2,pip5_s2,mcp1_t1,mcp1_s1,mcp1_t2,mcp1_s2,mcp2_t1,mcp2_s1,mcp2_t2,mcp2_s2,mcp3_t1,mcp3_s1,mcp3_t2,mcp3_s2,mcp4_t1,mcp4_s1,mcp4_t2,mcp4_s2,mcp5_t1,mcp5_s1,mcp5_t2,mcp5_s2,hip_t1,hip_s1,hip_t2,hip_s2,knee_t1,knee_s1,knee_t2,knee_s2,a_tt_t1,a_tt_s1,a_tt_t2,a_tt_s2,a_tc_t1,a_tc_s1,a_tc_t2,a_tc_s2,mtl_t1,mtl_s1,mtl_t2,mtl_s2,mtp1_t1,mtp1_s1,mtp1_t2,mtp1_s2,mtp2_t1,mtp2_s1,mtp2_t2,mtp2_s2,mtp3_t1,mtp3_s1,mtp3_t2,mtp3_s2,mtp4_t1,mtp4_s1,mtp4_t2,mtp4_s2,mtp5_t1,mtp5_s1,mtp5_t2,mtp5_s2,ip1b_t1,ip1b_s1,ip1b_t2,ip1b_s2,ip2b_t1,ip2b_s1,ip2b_t2,ip2b_s2,ip3b_t1,ip3b_s1,ip3b_t2,ip3b_s2,ip4b_t1,ip4b_s1,ip4b_t2,ip4b_s2,ip5b_t1,ip5b_s1,ip5b_t2,ip5b_s2,s1_t1,s1_s1,s1_t2,s1_s2,cervical_anky,cervical_flex,cervical_ext,cervical_rtrot,cervical_ltrot,cervical_rtfl,cervical_ltfl,cervical_pt,thorasic_anky,thorasic_flex,thorasic_ext,thorasic_rtrot,thorasic_ltrot,thorasic_rtfl,thorasic_ltfl,thorasic_pt,lumbar_anky,lumbar_flex,lumbar_ext,lumbar_rtrot,lumbar_ltrot,lumbar_rtfl,lumbar_ltfl,lumbar_pt,opt_rt,opt_lt,lcer_rt,lcer_lt,trpz_rt,trpz_lt,scap_rt,scap_lt,zcst_rt,zcst_lt,epdl_rt,epdl_lt,glut_rt,glut_lt,trcr_rt,trcr_lt,knee_rt,knee_lt,ta_rt,ta_lt,calf_rt,calf_lt,sole_rt,sole_lt,rhand_fl,rhand_ex,rhand_ab,rhand_add,lhand_fl,lhand_ex,lhand_ab,lhand_add,rwrist_fl,rwrist_ex,rwrist_ab,rwrist_add,lwrist_fl,lwrist_ex,lwrist_ab,lwrist_add,relb_fl,relb_ex,relb_ab,relb_add,shrt_fl,shrt_ex,shrt_ab,shrt_add,shlt_fl,shlt_ex,shlt_ab,shlt_add,kneert_fl,kneert_ex,kneert_ab,kneert_add,kneelt_fl,kneelt_ex,kneelt_ab,kneelt_add,footrt_fl,footrt_ex,footrt_ab,footrt_add,footlt_fl,footlt_ex,footlt_ab,footlt_add,thumb_rt,thumb_lt,finger_rt,finger_lt,palm_rt,palm_lt,elbow_rt,elbow_lt,hy_knee_rt,hy_knee_lt,ankle_rt,ankle_lt,other_rt,other_lt,spine_rt,spine_lt,can_u_dress_urself,can_u_wash_ur_hair,can_u_comb_ur_hair,can_u_stand_from_chair,can_u_get_inout_from_bed,can_u_sit_grossteg_onfloor,can_u_cut_vegetables,can_u_lift_glass,can_u_break_roti_from_1hand,can_u_walk,can_u_climb_5steps,can_u_take_bath,can_u_wash_dry_urbody,can_u_get_onoff_toilet,can_u_weigh_2kg,can_u_bend_and_pickcloths,can_u_open_bottle,can_u_turntaps_onoff,can_u_open_latches,can_u_work_office_house,can_u_run_errands,can_u_get_inout_of_bus,patient_assessment_pain,grip_strength_rt,grip_strength_hg,grip_strength_lt,early_mrng_stiffness,assess_sleep,general_health_assessment,classification_criteria_followig,diagnosis,treatment_plan,hematological_date,hem_esr,hem_hb,hem_tlc,hem_pleb,hem_plat,hem_urine,hem_others,hematological_date2,hem_esr2,hem_hb2,hem_tlc2,hem_pleb2,hem_plat2,hem_urine2,hem_others2,hematological_date3,hem_esr3,hem_hb3,hem_tlc3,hem_pleb3,hem_plat3,hem_urine3,hem_others3,hematological_date4,hem_esr4,hem_hb4,hem_tlc4,hem_pleb4,hem_plat4,hem_urine4,hem_others4,hematological_date5,hem_esr5,hem_hb5,hem_tlc5,hem_pleb5,hem_plat5,hem_urine5,hem_others5,biochemical_date,bio_lft,bio_rft,bio_crp,bio_bsl,bio_ua,bio_others,biochemical_date2,bio_lft2,bio_rft2,bio_crp2,bio_bsl2,bio_ua2,bio_others2,biochemical_date3,bio_lft3,bio_rft3,bio_crp3,bio_bsl3,bio_ua3,bio_others3,biochemical_date4,bio_lft4,bio_rft4,bio_crp4,bio_bsl4,bio_ua4,bio_others4,biochemical_date5,bio_lft5,bio_rft5,bio_crp5,bio_bsl5,bio_ua5,bio_others5,immunological_date,imm_rf,imm_accp,imm_ana,imm_anawb,imm_ace,imm_dsdna,imm_hlab27,imm_c3,imm_c4,imm_others,immunological_date2,imm_rf2,imm_accp2,imm_ana2,imm_anawb2,imm_ace2,imm_dsdna2,imm_hlab272,imm_c32,imm_c42,imm_others2,immunological_date3,imm_rf3,imm_accp3,imm_ana3,imm_anawb3,imm_ace3,imm_dsdna3,imm_hlab273,imm_c33,imm_c43,imm_others3,usg,xray_chest,xray_joints,mri,radio_remarks,das28,sledai,basdai,pasi,bvas,mrss,sdai_cdai,asdas,dapsa,vdi,essdai,user_id,
Output:
        {
  "RheumatologyPatient": {
    "a_tc_s1": null, 
    "a_tc_s2": null, 
    "a_tc_t1": null, 
    "a_tc_t2": null, 
    "a_tt_s1": null, 
    "a_tt_s2": null, 
    "a_tt_t1": null, 
    "a_tt_t2": null, 
    "abdomen_examination": "no", 
    "abortions": "yes", 
    "abortions_cause": "ipill", 
    "abortions_number": 2, 
    "acl_s1": "no", 
    "acl_s2": "no", 
    "acl_t1": "yes", 
    "acl_t2": "yes", 
    "addiction": "yes", 
    "address": "hyderabad", 
    "age": 12, 
    "alcohol": "YES", 
    "alcohol_years": 6, 
    "ankle_lt": null, 
    "ta_lt": null, 
    "ta_rt": null, 
    "temp": "98", 
    "tender": "najeeb", 
    "tender_type": "manshi", 
    "thorasic_anky": null, 
    "thorasic_ext": null, 
    "thorasic_flex": null, 
    "thorasic_ltfl": null, 
    "thorasic_ltrot": null, 
    "thorasic_pt": null, 
    "thorasic_rtfl": null, 
    "thorasic_rtrot": null, 
    "thumb_lt": null, 
    "thumb_rt": null, 
    "tm_s1": "no", 
    "tm_s2": "no", 
    "tm_t1": "yes", 
    "tm_t2": "yes", 
    "tobacco": "no", 
    "tobacco_years": 0, 
    "trcr_lt": null, 
    "trcr_rt": null, 
    "treatment_plan": null, 
    "trpz_lt": null, 
    "trpz_rt": null, 
    "user": {
      "dateofsub": "2021-03-02 16:29:33", 
      "email": "najeebaddu@gmail.com", 
      "id": 1, 
      "password": "2113", 
      "phone": 8121507936, 
      "role": "Doctor", 
      "speciality": "Breast Cancer", 
      "username": "najeeb pasha"
    }, 
    "user_id": 1, 
    "usg": null, 
    "vascular": "no", 
    "vdi": null, 
    "weight": 46, 
  }
}
#if fail:
    Fail
````
FOr rheuma patient follow:
````
http://localhost:5000/addfollowuprheuma

Input: check run.py for all inputs
below are few inputs in which pid,user_id and rheuma_id is
mandatory

{
  "pid" :  10,
  "code" : 48,
  "place" : "place",
  "pname" : "priyapota",
  "fhname" : "myname",
  "age" :22,
  "sex" :"F",
  "education" : "school",
  "user_id" : 2,
  "rheuma_id" : 1
}
output:
success ,fail
````
#Attachments
pid and file are hte inputs 

